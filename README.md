# Dawid Pytka, Dawid Kożuch #

System rezerwacji miejsc i sprzedaży biletów w kinie.

### Opis funkcjonalności ###

* Aplikacja pozwala wybrać jeden z dostępnych filmów, a następnie umożliwia rezerwacje wybranego przez użytkownika miejsca, oraz wydruk biletu.
* Dostępność miejsc na film jest przechowywana w pliku tekstowym w folderze movies.
* Wybór filmu odbywa się poprzez wskazanie odpowiedniego pliku z menu.
* Aplikacja sprawdza czy wszystkie miejsca są zajęte, jeśli nie, umożliwia kontynuowanie w celu rezerwacji. 
* Aplikacja automatycznie sprawdza poprawność wprowadzanych przez użytkownika danych, oraz dostępność wybranego miejsca.
* Po wybraniu miejsca użytkownik ma możliwość wydrukowania biletu do pliku tekstowego bilet.txt. Bilet wyświetla się również w konsoli.


### Instrukcja ###

* Po uruchomieniu aplikacji należy wybrać film na który chce się zarezerwować miejsce.
* Następnie aby kontynuować użytkownik musi wpisać cokolwiek w konsole, lub ‘X’, aby przerwać działanie programu. 
* Po przejściu dalej, użytkownik wybiera rząd oraz miejsce. Jeśli wprowadzone dane są poprawne, oraz miejsce jest wolne, zostaje ono zarezerwowane, a użytkownik ma możliwość wydrukowania biletu.
* Aplikacja od razu umożliwia rezerwacje kolejnego miejsca.


### Wykorzystane narzędzia ###

* Bitbucket
* Intellij
* Java
* Testowanie odbywało się poprzez sprawdzanie poprawności działania poszczególnych funkcji.


### Podział prac ###

* Dawid Pytka
* Klasa ticketReservation,
* Metody odpowiedzialne za wczytywanie pliku z miejscami,
* Metody odpowiedzialne za drukowanie biletu.

* Dawid Kożuch
* Klasa Repertuar,
* Logika w klasie main,
* Testowanie.
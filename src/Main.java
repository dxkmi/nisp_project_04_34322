import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.io.File;
import java.io.IOException;

class ticketReservation {

    private String[][] seats;
    private int rows;

    public static File selectedMovie;

    ticketReservation(String[][] s){
        this.seats = s;
        this.rows = s.length;
    }


    public void setSeats(String[][] seats) {
        this.seats = seats;
    }

    public static void setSelectedMovie(File sm){
        selectedMovie = sm;
    }

    public static File getSelectedMovie(){
        return selectedMovie;
    }

    public void setSeat(int x, int y){
        seats[x][y] = String.valueOf('X');
    }

    public String[][] getSeats() {
        return seats;
    }

    public int getRows(){
        return this.rows;
    }

    //Sprawdza czy sa wolne miejsca
    public boolean isFull() {
        for (String[] seat : seats) {
            for (String s : seat) {
                if (s.charAt(0) != 'X') {
                    return false;
                }
            }
        }
        return true;
    }

    //Wypisuje miejsca na ekran
    public void printSeats() {
        int row = 1;
        for (String[] seat : seats) {
            System.out.print(row++ + " ");
            for (String s : seat) {
                System.out.printf(s + " ");
            }
            if (row == seats.length + 1) {
                System.out.println("(VIP)");
            }
            System.out.println();
        }
    }

    //Zamienia character na cyfre
    public static int getIndex(char character) {
        switch (character) {
            case 'A':
                return 0;
            case 'B':
                return 1;
            case 'C':
                return 2;
            case 'D':
                return 3;
            default:
                return 4;
        }
    }

    public static void writeData(String[][] dataIn) throws IOException {

        File f = new File(selectedMovie.toString());
        FileWriter fw = null;
        try {
            fw = new FileWriter (f, false);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            for (String[] strings : dataIn) {
                for (String string : strings) {
                    fw.write(string + ", ");
                }
                fw.write(System.getProperty("line.separator"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        fw.close();
    }

}

class Repertuar {

    private static List<String> movies;
    public static List<String> getMovies() {
        return movies;
    }

    public static void setMovies() throws IOException {
        List<String> results = new ArrayList<String>();

        File[] files = new File("./src/movies").listFiles();


        assert files != null;
        for (File file : files) {
            if (file.isFile()) {
                results.add(file.getName());
            }
        }
        Repertuar.write("./src/movies.txt", results);
        Repertuar.movies = results;
    }

    public static void write (String filename, List<String> x) throws IOException{
        FileWriter writer = new FileWriter(filename);
        for (String str: x) {
            writer.write(str + System.lineSeparator());
        }
        writer.close();
    }

    @Override
    public String toString() {
        List<String> m = null;
        String mvs = "";

        for (String x: movies){
            String[] segments = x.split("\\.");
            mvs += segments[0] + ", ";
        }

        return mvs.substring(0, mvs.length()-2);
    }
}

public class Main {

    private static final int ROWS = 4;
    private static final int COLS = 4;
    public static void main(String[] args) throws IOException {
        Repertuar rep = new Repertuar();
        Repertuar.setMovies();
        System.out.println(rep.toString());
        Scanner scaner = new Scanner(System.in);
        ticketReservation movie_1 = new ticketReservation(readFile());
        while(!movie_1.isFull()){
            movie_1.printSeats();
            System.out.println("Wpisz cokolwiek aby przejsc dalej, wpisz 'X' aby zakończyć");
            char check = Character.toUpperCase(scaner.nextLine().charAt(0));
            if (check == 'X') {
                System.out.println("Koniec");
                System.exit(0);
            }

            System.out.println("Podaj rząd");
            int row = 0;
            boolean rowTaken = false;
            while (!rowTaken) {
                int checkRow = Character.getNumericValue(scaner.nextLine().charAt(0));
                if (checkRow >= 1 && checkRow <= movie_1.getRows()) {
                    row = checkRow;
                    rowTaken = true;
                } else {
                    System.out.println("Źle, podaj pomiędzy 1, a "+ movie_1.getRows());
                }
            }

            System.out.println("Wskaż miejsce które chcesz zarezerwować");
            char column = ' ';
            boolean columnTaken = false;
            while (!columnTaken) {
                char checkColumn = Character.toUpperCase(scaner.nextLine().charAt(0));
                if (checkColumn >= 'A' && checkColumn <= 'D') {
                    column = checkColumn;
                    columnTaken = true;
                } else {
                    System.out.println("Źle, podaj odpowiednie miejsce");
                }
            }

            if (movie_1.getSeats()[row - 1][ticketReservation.getIndex(column)].charAt(0) == 'X') {
                System.out.println("To miejsce jest zajęte");
            } else {
                //movie_1.getSeats()[row - 1][ticketReservation.getIndex(column)] = String.valueOf('X');
                movie_1.setSeat(row-1, ticketReservation.getIndex(column));
                //movie_1.getSeats();
                ticketReservation.writeData(movie_1.getSeats());
                System.out.println("Miejsce " + row + column + " zostalo zarezerwowane");
                System.out.println("Czy chcesz wydrukowac bilet? T/N");

                char checkPrint = Character.toUpperCase(scaner.nextLine().charAt(0));
                if (checkPrint == 'T'){
                    ticketToFile(row, column);
                    System.out.println(printTicket(row, column));

                } else {
                    System.out.println("Koniec");
                }

            }scaner.nextLine();
        }
    }

    private static String[][] readFile() throws IOException {
        String[][] seats = new String[ROWS][COLS];
        Scanner sc = new Scanner(chooseTextFile());
        while (sc.hasNextLine()){
            for (String[] seat : seats) {
                String[] line = sc.nextLine().trim().split("," + " ");
                System.arraycopy(line, 0, seat, 0, line.length);
            }
        }
        return seats;
    }

    private static File chooseTextFile() {
        FileDialog dialog = new FileDialog((Frame) null, "Wybierz film");
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        File[] file = dialog.getFiles();
        dialog.setDirectory("./src/movies");
        ticketReservation.setSelectedMovie(file[0]);
        return file[0];
    }

    private static void ticketToFile(int row, char col) throws IOException {
        File ticket = new File("./src/bilet.txt");
        FileWriter fw = new FileWriter(ticket);
        fw.write(printTicket(row, col));
        fw.close();
    }

    private static String printTicket(int row, char col) {
        String movie = String.valueOf(ticketReservation.getSelectedMovie());
        String newstr = movie;
        if (null != movie && movie.length() > 0 )
        {
            int endIndex = movie.lastIndexOf('\\');
            if (endIndex != -1)
            {
                newstr = movie.substring(endIndex+1);
            }
        }
        newstr = newstr.split("\\.")[0];
        String seat = row + "" + col;
        return "__________________\n"+"TWÓJ BILET NA FILM\n"+ newstr + "\n"+ "MIEJSCE:\n"+ seat + "\n"+ "__________________";
    }
}